Sub Create_Tables()

DoCmd.RunSQL "CREATE TABLE Client(" & _
   "IdClient VARCHAR(50)," & _
   "AdrClient VARCHAR(50)," & _
   "PrenomClient VARCHAR(50)," & _
   "CPclient VARCHAR(50)," & _
   "MailClient VARCHAR(50)," & _
   "DateNaissClient DATE," & _
   "NomPrenom VARCHAR(50)," & _
   "TelClient BIGINT," & _
   "PRIMARY KEY(IdClient)" & _
");"   

DoCmd.RunSQL "CREATE TABLE Restaurant(" & _
   "NomResto VARCHAR(50)," & _
   "Sp�cialit� VARCHAR(50)," & _
   "Capacite VARCHAR(50)," & _
   "PRIMARY KEY(NomResto)" & _
");"   

DoCmd.RunSQL "CREATE TABLE Soci�t�(" & _
   "NumSIRET INT," & _
   "RaisSocial VARCHAR(50)," & _
   "NumSIRET_1 INT NOT NULL," & _
   "PRIMARY KEY(NumSIRET)," & _
   "FOREIGN KEY(NumSIRET_1) REFERENCES Soci�t�(NumSIRET)" & _
");"   

DoCmd.RunSQL "CREATE TABLE Chambre(" & _
   "NumChambre VARCHAR(50)," & _
   "NbCuisine INT," & _
   "NbLitDouble INT," & _
   "NbLitSimple INT," & _
   "Television VARCHAR(50)," & _
   "PRIMARY KEY(NumChambre)" & _
");"   

DoCmd.RunSQL "CREATE TABLE Parking(" & _
   "NumParking VARCHAR(50)," & _
   "Situation VARCHAR(50)," & _
   "PRIMARY KEY(NumParking)" & _
");"   

DoCmd.RunSQL "CREATE TABLE Batiment(" & _
   "NumBat VARCHAR(50)," & _
   "NomBat VARCHAR(50)," & _
   "PRIMARY KEY(NumBat)" & _
");"   

DoCmd.RunSQL "CREATE TABLE Appartement(" & _
   "NumAppt VARCHAR(50)," & _
   "NbCuisine VARCHAR(50)," & _
   "PRIMARY KEY(NumAppt)" & _
");"   

DoCmd.RunSQL "CREATE TABLE Hotel(" & _
   "NumBat VARCHAR(50)," & _
   "CodeHotel VARCHAR(50)," & _
   "NbEtoile INT NOT NULL," & _
   "AdrHotel VARCHAR(50)," & _
   "NomHotel VARCHAR(50) NOT NULL," & _
   "NumSIRET INT NOT NULL," & _
   "PRIMARY KEY(NumBat, CodeHotel)," & _
   "FOREIGN KEY(NumBat) REFERENCES Batiment(NumBat)," & _
   "FOREIGN KEY(NumSIRET) REFERENCES Soci�t�(NumSIRET)" & _
");"   

DoCmd.RunSQL "CREATE TABLE Agent(" & _
   "IdAgent VARCHAR(50)," & _
   "PrenomAgent VARCHAR(50)," & _
   "NomAgent VARCHAR(50)," & _
   "NumBat VARCHAR(50) NOT NULL," & _
   "CodeHotel VARCHAR(50) NOT NULL," & _
   "PRIMARY KEY(IdAgent)," & _
   "FOREIGN KEY(NumBat, CodeHotel) REFERENCES Hotel(NumBat, CodeHotel)" & _
");"   

DoCmd.RunSQL "CREATE TABLE Reservation(" & _
   "NumResa VARCHAR(50)," & _
   "NbParking INT," & _
   "DateFinSejour DATETIME," & _
   "DateDebSejour DATETIME," & _
   "NbOccuppant INT," & _
   "TarifResa CURRENCY," & _
   "NumBat VARCHAR(50) NOT NULL," & _
   "CodeHotel VARCHAR(50) NOT NULL," & _
   "IdClient VARCHAR(50) NOT NULL," & _
   "IdAgent VARCHAR(50) NOT NULL," & _
   "PRIMARY KEY(NumResa)," & _
   "FOREIGN KEY(NumBat, CodeHotel) REFERENCES Hotel(NumBat, CodeHotel)," & _
   "FOREIGN KEY(IdClient) REFERENCES Client(IdClient)," & _
   "FOREIGN KEY(IdAgent) REFERENCES Agent(IdAgent)" & _
");"   

DoCmd.RunSQL "CREATE TABLE Facture(" & _
   "NumFacture VARCHAR(50)," & _
   "DateFacture DATE," & _
   "IdClient VARCHAR(50) NOT NULL," & _
   "NumResa VARCHAR(50) NOT NULL," & _
   "PRIMARY KEY(NumFacture)," & _
   "FOREIGN KEY(IdClient) REFERENCES Client(IdClient)," & _
   "FOREIGN KEY(NumResa) REFERENCES Reservation(NumResa)" & _
");"   

DoCmd.RunSQL "CREATE TABLE poss�de(" & _
   "NumBat VARCHAR(50)," & _
   "CodeHotel VARCHAR(50)," & _
   "NomResto VARCHAR(50)," & _
   "NumParking VARCHAR(50)," & _
   "PRIMARY KEY(NumBat, CodeHotel, NomResto, NumParking)," & _
   "FOREIGN KEY(NumBat, CodeHotel) REFERENCES Hotel(NumBat, CodeHotel)," & _
   "FOREIGN KEY(NomResto) REFERENCES Restaurant(NomResto)," & _
   "FOREIGN KEY(NumParking) REFERENCES Parking(NumParking)" & _
");"   

DoCmd.RunSQL "CREATE TABLE se_trouve_dans(" & _
   "NumBat VARCHAR(50)," & _
   "CodeHotel VARCHAR(50)," & _
   "NumChambre VARCHAR(50)," & _
   "NumAppt VARCHAR(50)," & _
   "PRIMARY KEY(NumBat, CodeHotel, NumChambre, NumAppt)," & _
   "FOREIGN KEY(NumBat, CodeHotel) REFERENCES Hotel(NumBat, CodeHotel)," & _
   "FOREIGN KEY(NumChambre) REFERENCES Chambre(NumChambre)," & _
   "FOREIGN KEY(NumAppt) REFERENCES Appartement(NumAppt)" & _
");"   

DoCmd.RunSQL "CREATE TABLE est_dans_(" & _
   "NumChambre VARCHAR(50)," & _
   "NumAppt VARCHAR(50)," & _
   "PRIMARY KEY(NumChambre, NumAppt)," & _
   "FOREIGN KEY(NumChambre) REFERENCES Chambre(NumChambre)," & _
   "FOREIGN KEY(NumAppt) REFERENCES Appartement(NumAppt)" & _
");"   

DoCmd.RunSQL "CREATE TABLE est_jumul�_�(" & _
   "NumChambre VARCHAR(50)," & _
   "NumChambre_1 VARCHAR(50)," & _
   "PRIMARY KEY(NumChambre, NumChambre_1)," & _
   "FOREIGN KEY(NumChambre) REFERENCES Chambre(NumChambre)," & _
   "FOREIGN KEY(NumChambre_1) REFERENCES Chambre(NumChambre)" & _
");"   

DoCmd.RunSQL "CREATE TABLE r�gle_(" & _
   "NumFacture VARCHAR(50)," & _
   "acompte INT," & _
   "IdClient VARCHAR(50) NOT NULL," & _
   "PRIMARY KEY(NumFacture)," & _
   "FOREIGN KEY(NumFacture) REFERENCES Facture(NumFacture)," & _
   "FOREIGN KEY(IdClient) REFERENCES Client(IdClient)" & _
");"   

End Sub